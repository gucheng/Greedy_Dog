import pygame
import sys
from pygame.locals import *

# put the bone into its mouth and press mousebutton, the dog will start to eat

class Dog:

    def __init__(self):
        self.earRGB = (104, 66, 12)
        self.faceRGB = (183, 144, 25)
        self.mouthRGB = (201, 69, 40)

        self.boneImg = pygame.image.load("bone.png")
        self.boneImg = pygame.transform.scale(self.boneImg, (100, 50))

        self.isMousePressed = False

        self.move_x = 0.0
        self.move_y = 0.0

        self.x = 0
        self.y = 0

        self.space_x = 30
        self.space_y = 15

        self.mouthMove = 0

        self.boneHP = 100
        self.hasBone = True

        self.nextTime=pygame.time.get_ticks()

        # self.angle_L = 10
        # self.angle_R = -10

    def updateDog(self):
        self.draw()
        self.animation()

    def draw(self):

        # Face
        pygame.draw.ellipse(screen, self.faceRGB, (100, 100, 300, 360))

        # Ears
        vertexList_L = [[130 - self.move_x, 90], [180, 120 - self.move_y],
                        [80 - self.move_x, 300 + self.move_y], [10 - self.move_x, 260 + self.move_y]]
        vertexList_R = [[370 + self.move_x, 90], [320, 120 - self.move_y],
                        [420 + self.move_x, 300 + self.move_y], [490 + self.move_x, 260 + self.move_y]]
        pygame.draw.polygon(screen, self.earRGB, vertexList_L)
        pygame.draw.polygon(screen, self.earRGB, vertexList_R)

        # Mouth
        pygame.draw.ellipse(screen, self.mouthRGB,
                            (235, 335, 30, 10 + self.mouthMove))
        pygame.draw.rect(screen, self.faceRGB, (200, 250, 100, 130))

        # Nose
        pygame.draw.ellipse(screen, blackRGB, (225, 300, 50, 30))
        pygame.draw.line(screen, blackRGB, [250, 325], [250, 370], 5)
        pygame.draw.lines(screen, blackRGB, False, [
                          [220, 390], [250, 370], [280, 390]], 5)
        for i in range(0, 2):
            for j in range(0, 3):
                pygame.draw.circle(
                    screen, blackRGB, (235 + self.space_x * i, 335 + self.space_y * j), 3)

        # Eyes
        pygame.draw.circle(screen, whiteRGB, (180, 250), 35)
        pygame.draw.circle(screen, whiteRGB, (320, 250), 35)
        pygame.draw.circle(screen, blackRGB, (180 + self.x, 250 + self.y), 10)
        pygame.draw.circle(screen, blackRGB, (320 + self.x, 250 + self.y), 10)

    def animation(self):

        # Mouse move event
        mouseX, mouseY = pygame.mouse.get_pos()
        if self.hasBone:
            screen.blit(self.boneImg, (mouseX - 50, mouseY - 25))
        
        if self.boneHP==0:
            #self.nextTime=pygame.time.get_ticks()
            if  pygame.time.get_ticks()>= self.nextTime + 10000:
                self.hasBone = True
                self.boneHP = 100
                self.nextTime = pygame.time.get_ticks()

        if mouseX > 320 and self.x <= 20:
            self.x += 4
        elif mouseX < 180 and self.x >= -20:
            self.x -= 4

        if mouseY > 250 and self.y <= 20:
            self.y += 4
        elif mouseY < 250 and self.y >= -20:
            self.y -= 4

        if mouseX > 200 and mouseX < 300:
            if self.x > 0:
                self.x -= 4
            if self.x < 0:
                self.x += 4

        # Mouse press event
        if self.isMousePressed:
            self.move_x += 1.0
            self.move_y += 0.5
            if self.move_x >= 10 or self. move_y >= 5:
                self.move_x = 10
                self.move_y = 5

            if mouseX > 200 and mouseX < 300 and self.hasBone:
                self.mouthMove += 10
                if self.mouthMove > 80:
                    self.mouthMove = 60

                self.boneHP -= 1
                if self.boneHP <= 0:
                    self.hasBone = False
            else:
                self.mouthMove = 0

        else:
            self.move_x -= 1.0
            self.move_y -= 0.5
            if self.move_x <= 0 or self.move_y <= 0:
                self.move_x = 0
                self.move_y = 0

            self.mouthMove -= 10
            if self.mouthMove < 0:
                self.mouthMove = 0

        # if self.isMousePressed:
        #     self.angle_L -= 1
        #     if self.angle_L < 0:
        #         self.angle_L = 0

        #     self.angle_R += 1
        #     if self.angle_R > 0:
        #         self.angle_R = 0
        # elif self.isMousePressed == False:
        #     self.angle_L += 1
        #     if self.angle_L > 10:
        #         self.angle_L = 10

        #     self.angle_R -= 1
        #     if self.angle_R <-10:
        #         self.angle_R =-10

        # rotatedSurf_L = pygame.transform.rotate(earSurf_L, self.angle_L)
        # rotatedSurf_R= pygame.transform.rotate(rotatedSurf_R, self.angle_R)
        # screen.blit(rotatedSurf_L, (0, 0))
        # screen.blit(rotatedSurf_R, (260, 0))

    def isMouseDown(self, pressed):
        self.isMousePressed = pressed


pygame.init()
pygame.display.set_caption("Dog")
screen = pygame.display.set_mode((500, 600))
# earSurf_L = pygame.Surface((500, 600))
# earSurf_R = pygame.Surface((500, 600))

# colors
whiteRGB = (220, 220, 220)
blackRGB = (50, 50, 50)

FPS = 30
fpsClock = pygame.time.Clock()

# new a instance for class Dog
myDog = Dog()

# Main game loop
while True:
    # earSurf_L.fill(whiteRGB)
    # earSurf_L.set_colorkey(whiteRGB)
    # earSurf_R.fill(whiteRGB)
    # earSurf_R.set_colorkey(whiteRGB)

    screen.fill(blackRGB)
    myDog.updateDog()

    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == pygame.MOUSEBUTTONDOWN:
            myDog.isMouseDown(True)
        elif event.type == pygame.MOUSEBUTTONUP:
            myDog.isMouseDown(False)

    pygame.display.update()
    fpsClock.tick(FPS)
